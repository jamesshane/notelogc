export const sqrt = Math.sqrt;
export function square(x) {
    return x * x;
}
export function diag(x, y) {
    return sqrt(square(x) + square(y));
}
export function deleteNoteConfirm() { 
    //var doc; 
    var result = window.confirm("Are you sure you want to delete this note?"); 
    if (result === true) { 
        //doc = "OK was pressed."; 
        return true;
    } else { 
        //doc = "Cancel was pressed."; 
        return false;
    } 
    //window.document.getElementById("g").innerHTML = doc; 
} 
export function crtobr(x) {
    return x.replace(/\n/gi,'<br />');
}