import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './JotCow.css';

import CreateNote from './components/CreateNote';
import ShowNoteList from './components/ShowNoteList';
import ShowNoteDetails from './components/ShowNoteDetails';
import UpdateNoteInfo from './components/UpdateNoteInfo';
import ShowRawNoteDetails from './components/ShowRawNoteDetails';


class App extends Component {
  render() {
    return (
      <Router>
        <div className="routerbody">
          <div className="alert alert-danger" role="alert">
            This project is alpha-dev, what you use will be removed until it is out of dev.
          </div>  
          <Route exact path='/' component={ShowNoteList} />
          <Route path='/create-Note' component={CreateNote} />
          <Route path='/edit-Note/:id' component={UpdateNoteInfo} />
          <Route exact path='/show-Note/:id' component={ShowNoteDetails} />
          <Route exact path='/show-Raw-Note/:id' component={ShowRawNoteDetails} />
        </div>
      </Router>
    );
  }
}

export default App;
