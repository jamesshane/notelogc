import React from 'react';
import { Link } from 'react-router-dom';

const NoteCard = (props) => {
    const  note  = props.note;

    return(
        <div className="card-container">
            <div className="desc">
                <h2>
                    <Link to={`/show-note/${note._id}`}>
                        { note.title }
                    </Link>
                </h2>
                <h3>{note.description}</h3>
                <p>{note.updated_date}</p>
            </div>
        </div>
    )
};

export default NoteCard;