import React, { Component } from 'react';
import axios from 'axios';
import { myConfig } from '../Config';

class ShowRawNoteDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      note: {}
    };
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get(myConfig.apiURL+'/notes/'+this.props.match.params.id)
      .then(res => {
        // console.log("Print-showNoteDetails-API-response: " + res.data);
        this.setState({
          note: res.data
        })
      })
      .catch(err => {
        console.log("Error from ShowRawNoteDetails");
      })
  };


  render() {
    const note = this.state.note; 
    const code = note.note;
    let NoteItem = 
    <div className="showRawText">
    { code }
    </div>

    return (
      <div>
      { NoteItem }
      </div>
    );
  }
}

export default ShowRawNoteDetails;