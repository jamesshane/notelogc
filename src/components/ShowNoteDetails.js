import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Prism from 'prismjs';
import $ from 'jquery';
import { myConfig } from '../Config';
import { square, diag, deleteNoteConfirm } from '../Jotcow';

class ShowNoteDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      note: {}
    };
    this.linkref = {};
  }

  onChange = e => {
    this.linkref.linkref = e.target.value;
    $(".linklabel").text(e.target.value);
    //this.setState({ [e.target.name]: e.target.value });
  };

  componentDidUpdate() {
    console.log(square(11)); // 121
    console.log(diag(4, 3)); // 5
    // You can call the Prism.js API here
    // Use setTimeout to push onto callback queue so it runs after the DOM is updated
    setTimeout(() => Prism.highlightAll(), 0)
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get(myConfig.apiURL+'/notes/'+this.props.match.params.id)
      .then(res => {
        // console.log("Print-showNoteDetails-API-response: " + res.data);
        this.setState({
          note: res.data
        })
      })
      .catch(err => {
        console.log("Error from ShowNoteDetails");
      })

      axios
        .get(myConfig.apiURL+'/linkrefs/'+this.props.match.params.id)
        .then(res => {
          // console.log("Print-showNoteDetails-API-response: " + res.data);
          this.linkref = {
            linkref: res.data
          }
        })
        .catch(err => {
          console.log("Error from ShowNoteDetails");
        })
        console.log(JSON.stringify(this.linkref));
  };

  onDeleteClick (id) {
    if(deleteNoteConfirm()) {
      axios
        .delete(myConfig.apiURL+'/notes/'+id)
        .then(res => {
          this.props.history.push("/");
        })
        .catch(err => {
          console.log("Error form ShowNoteDetails_deleteClick");
        })
    }
  };  
  
  onSubmitLinkEdit = e => {
    $(".linkedit").toggleClass('d-none');
    $(".linksave").toggleClass('d-none');
  };

  onSubmitLinkCancel = e => {
    $(".linkedit").toggleClass('d-none');
    $(".linksave").toggleClass('d-none');
  };
  
  onSubmitLink = e => {
    //TODO: check if link in new or update
    e.preventDefault();

    const data = {
      label: this.linkref.linkref,
      noteid: this.state.note._id,
      linkref: this.linkref.linkref
    };

    console.log(JSON.stringify(data));

    axios
      .post(myConfig.apiURL+'/linkrefs', data)
      .then(res => {
        this.linkref={
          label: '',
          noteid: '',
          linkref: ''
        };
        //this.props.history.push('/');
        $(".linkedit").toggleClass('d-none');
        $(".linksave").toggleClass('d-none');
      }
      )
      .catch(err => {
        console.log("Error in CreateLink!");
      })
  };


  render() {

    const note = this.state.note; 
    const code = note.note;
    const linkref=this.linkref;
    let LinkItem = 
    <div className="row">

      <div className="col-md-12 linksave d-none">
        <input
          type='text'
          placeholder='Link URL'
          name='linkref'
          id={linkref._id}
          className='form-control linktext'
          onChange={this.onChange}
        />
        <br />
        <Link to="#" onClick={this.onSubmitLink} className="btn btn-outline-secondary">
          Save
        </Link>
        <Link to="#" onClick={this.onSubmitLinkCancel} className="btn btn-outline-info">
          Cancel
        </Link>
      </div>

      <div className="col-md-12 linkedit">
        <div className="linklabel font-weight-bold text-center">
          
        </div>
        <br />
        <Link to="#" onClick={this.onSubmitLinkEdit} className="btn btn-outline-secondary">
          Edit
        </Link>
      </div>

      <div className="col-md-6 linkadd d-none">
        <Link to="/" className="btn btn-outline-secondary">
          + Add Link
        </Link>
      </div>

    </div>

    let NoteItem = 
    <div>
      <table className="table table-striped table-light">
        {/* <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead> */}
        <tbody>
          <tr>
            <th scope="row">Title</th>
            <td>{ note.title }</td>
          </tr>
          <tr>
            <th scope="row">Description</th>
            <td>{ note.description }</td>
          </tr>
          <tr>
            <th scope="row">Date</th>
            <td>{ note.updated_date }</td>
          </tr>
          <tr>
            <td colSpan="2">
              { /*<textarea
                rows='20'
                type='text'
                className='form-control'
                value={ note.note }
                readOnly>
              </textarea> 
              <br /> */ }
              <div className="showRawTextBadge">
                <Link to={`/show-raw-note/${note._id}`} className="badge badge-light float-right" target="_blank">
                  Show Raw Text
                </Link>
              </div>
              <pre className="line-numbers form-control gridcode">
                <code className="language-textfile content">
                  {code}
                </code>
              </pre>
            </td>
          </tr>
          <tr className="d-none">
            <td colSpan="2">
              { LinkItem }
            </td>
          </tr>
        </tbody>
      </table>
    </div>


    return (
      <div className="ShowNoteDetails">
        <div className="container">
          <div className="row">
            <div className="col-md-10 m-auto">
              <br /> <br />
              <Link to="/" className="btn btn-outline-secondary float-left">
                  Show Note List
              </Link>
            </div>
            <br />
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">JotCow's Record</h1>
              <p className="lead text-center">
                  View Note's Info
              </p>
              <hr /> <br />
            </div>
          </div>
          <div className="NoteInfo">
            { NoteItem }
          </div>


          <div className="row">
            <div className="col-md-6">
              <button type="button" className="btn btn-outline-danger btn-lg btn-block" onClick={this.onDeleteClick.bind(this,note._id)}>Delete Note</button><br />
            </div>

            <div className="col-md-6">
              <Link to={`/edit-note/${note._id}`} className="btn btn-outline-secondary btn-lg btn-block">
                    Edit Note
              </Link>
            </div>

          </div>
            {/* <br />
            <button type="button" class="btn btn-outline-info btn-lg btn-block">Edit Note</button>
            <button type="button" class="btn btn-outline-danger btn-lg btn-block">Delete Note</button> */}

        </div> 
          <p id="g"></p> 
        <br />
      </div>
    );
  }
}

export default ShowNoteDetails;