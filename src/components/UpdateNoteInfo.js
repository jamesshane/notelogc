import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { myConfig } from '../Config';

class UpdateNoteInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description:'',
      note:''
    };
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get(myConfig.apiURL+'/notes/'+this.props.match.params.id)
      .then(res => {
        // this.setState({...this.state, note: res.data})
        this.setState({
          title: res.data.title,
          description: res.data.description,
          note: res.data.note
        })
      })
      .catch(err => {
        console.log("Error from UpdateNoteInfo");
      })
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      title: this.state.title,
      description: this.state.description,
      note: this.state.note
    };

    axios
      .put(myConfig.apiURL+'/notes/'+this.props.match.params.id, data)
      .then(res => {
        this.props.history.push('/show-note/'+this.props.match.params.id);
      })
      .catch(err => {
        console.log("Error in UpdateNoteInfo!");
      })
  };

  render() {
    return (
      <div className="UpdateNoteInfo">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-secondary float-left">
                  Show Note List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Edit JotCow Note</h1>
              <p className="lead text-center">
                  Update Note's Info
              </p>
            </div>
          </div>

          <div className="col-md-8 m-auto">
          <form noValidate onSubmit={this.onSubmit}>
            <div className='form-group'>
              <label htmlFor="title">Title</label>
              <input
                type='text'
                placeholder='Title of the Note'
                name='title'
                className='form-control'
                value={this.state.title}
                onChange={this.onChange}
              />
            </div>
            <br />

            <div className='form-group'>
            <label htmlFor="description">Description</label>
              <input
                type='text'
                placeholder='Description'
                name='description'
                className='form-control'
                value={this.state.description}
                onChange={this.onChange}
              />
            </div>

            <div className='form-group'>
            <label htmlFor="note">Note</label>
              <textarea
                rows="20"
                type='text'
                placeholder='Note of this Note'
                name='note'
                className='form-control editta'
                value={this.state.note}
                onChange={this.onChange}
              />
            </div>
            <br />
            
            <button type="submit" className="btn btn-outline-secondary btn-lg btn-block">Update Note</button>
            <br />
            </form>
          </div>

        </div>
      </div>
    );
  }
}

export default UpdateNoteInfo;