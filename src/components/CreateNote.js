import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { myConfig } from '../Config';


class CreateNote extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      description:'',
      note:''
    };
  }

  componentDidUpdate() {
          // Example starter JavaScript for disabling form submissions if there are invalid fields
          (function() {
            //'use strict';
            window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = window.document.getElementsByClassName('needs-validation');
              console.log("in val event");
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                  }
                  form.classList.add('was-validated');
                }, false);
              });
            }, false);
          })();
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      title: this.state.title,
      description: this.state.description,
      note: this.state.note
    };

    axios
      .post(myConfig.apiURL+'/notes', data)
      .then(res => {
        this.setState({
          title: '',
          description:'',
          note:''
        })
        this.props.history.push('/');
      })
      .catch(err => {
        console.log("Error in CreateNote!");
      })
  };

  render() {
    return (
      <div className="CreateNote">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-secondary float-left">
                  Show Note List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Add JotCow Note</h1>
              <p className="lead text-center">
                  Create new note
              </p>

              <form noValidate className="needs-validation" onSubmit={this.onSubmit}>
                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Title of the Note (required)'
                    name='title'
                    id="validationCustom01"
                    className='form-control'
                    value={this.state.title}
                    onChange={this.onChange}
                    required
                  />
                  <div className="valid-feedback">
                    Looks good!
                  </div>
                  <div className="invalid-feedback">
                    Doesn't look good!
                  </div>
                </div>
                <br />

                <div className='form-group'>
                  <input
                    type='text'
                    placeholder='Describe this note'
                    name='description'
                    className='form-control'
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </div>

                <div className='form-group'>
                  <textarea
                    rows='20'
                    type='text'
                    placeholder='Note on this Note (required)'
                    name='note'
                    className='form-control editta'
                    value={this.state.note}
                    onChange={this.onChange}
                    required
                ></textarea>
                </div>

                <input
                    type="submit"
                    value="Add Note"
                    className="btn btn-outline-secondary btn-block mt-4"
                />
                <br />
              </form>
          </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateNote;