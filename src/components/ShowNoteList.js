import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import NoteCard from './NoteCard';
import { myConfig } from '../Config';

class ShowNoteList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notes: []
    };
  }

  componentDidMount() {
    axios
      .get(myConfig.apiURL+'/notes')
      .then(res => {
        this.setState({
          notes: res.data
        })
      })
      .catch(err =>{
        console.log('Error from ShowNoteList');
      })
  };


  render() {
    const notes = this.state.notes;
    console.log("Print Note: " + notes);
    let noteList;

    if(!notes) {
      noteList = "there is no note record!";
    } else {
      noteList = notes.map((note, k) =>
        <NoteCard note={note} key={k} />
      );
    }

    return (
      <div className="ShowNoteList">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <br />
              <h2 className="display-4 text-center">JotCow Notes List</h2>
            </div>

            <div className="col-md-11">
              <Link to="/create-note" className="btn btn-outline-secondary float-right">
                + Add New Note
              </Link>
              <br />
              <br />
              <hr />
            </div>

          </div>

          <div className="list">
                {noteList}
          </div>
        </div>
      </div>
    );
  }
}

export default ShowNoteList;